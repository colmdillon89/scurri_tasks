

def transform_three_five(i):
    """
    Takes an integer and returns "Three" if multiple of 3 and "Five" if multiple of 5
    and "ThreeFive" if multiple of both 3 and 5.
    :param i: (int) an integer to check.
    :return: (str) a number string.

    """

    r_val = ""

    if i % 3 == 0:
        r_val += "Three"
    if i % 5 == 0:
        r_val += "Five"

    if not r_val:
        return str(i)
    else:
        return r_val


def main():

    for i in range(1, 100):
        print(transform_three_five(i))


if __name__ == '__main__':
    main()
