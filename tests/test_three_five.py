from unittest import TestCase
from three_five import transform_three_five


class ThreeFiveTestCase(TestCase):

    def test_three_five_returns_Three_when_value_is_multiple_of_three(self):
        test_val = 33

        self.assertEqual(transform_three_five(test_val), "Three")

    def test_three_five_returns_Five_when_value_is_multiple_of_five(self):
        test_val = 55

        self.assertEqual(transform_three_five(test_val), "Five")

    def test_three_five_returns_ThreeFive_when_value_is_multiple_of_both_three_and_five(self):
        test_val = 30

        self.assertEqual(transform_three_five(test_val), "ThreeFive")

    def test_three_five_returns_number_when_value_is_not_multiple_of_three_or_five(self):
        test_val = 1

        self.assertEqual(transform_three_five(test_val), "1")