from unittest import TestCase
from parameterized import parameterized
from uk_postcodes import UkPostcode, InvalidPostcode


class UkPostcodesTestCase(TestCase):

    @parameterized.expand([
        ("valid_pc", "sw1w 0ny", "SW1W 0NY"),
        ("another_valid_pc", "sw1w0ny", "SW1W 0NY"),
    ])
    def test_valid_postcode_format(self, test_case, postcode, expected):

        pc = UkPostcode(postcode).format()

        self.assertEqual(pc, expected)

    @parameterized.expand([
        ("invalid_zero_district_area", "BL3 0NY", InvalidPostcode),
        ("invalid_zero_district_area_WC", "BS5 0NY", InvalidPostcode),
        ("invalid_double_digit_area_AB", "AB1 0NY", InvalidPostcode),
        ("invalid_double_digit_area_SO", "SO9 0NY", InvalidPostcode),
        ("invalid_single_digit_area", "BR12 0NY", InvalidPostcode),
        ("invalid_single_digit_area_WC", "WC11 0NY", InvalidPostcode),
        ("invalid_pc_too_long", "THIS IS AN INVALID POST_CODE", InvalidPostcode),
        ("invalid_pc", "1111 111", InvalidPostcode),
        ("invalid_pc_symbol_inward", "BL0 ?NY", InvalidPostcode),
        ("invalid_pc_symbol_outward", "B?0 0NY", InvalidPostcode),
        ("invalid_london_pc_EC", "EC23 0NY", InvalidPostcode),
        ("invalid_london_pc_SE1", "SE1Z 0NY", InvalidPostcode),
        ("invalid_london_pc_NW1", "NW1Q 0NY", InvalidPostcode),
        ("invalid_london_pc_N1", "N1Z 0NY", InvalidPostcode),
        ("invalid_london_pc_E1", "E1Z 0NY", InvalidPostcode),
        ("invalid_london_pc_WC2", "WC29 0NY", InvalidPostcode)
    ])
    def test_check_is_valid_raises_AssertionError(self, test_case, postcode, expected):

        pc = UkPostcode(postcode)

        self.assertRaises(expected, pc.is_valid)

    @parameterized.expand([
        ("valid_district_zero_area", "BL0 0NY", True),
        ("valid_district_zero_area_BS_zero", "BS0 0NY", True),
        ("valid_district_zero_area_BS_ten", "BS10 0NY", True),
        ("valid_double_digit_area", "AB12 0NY", True),
        ("valid_double_digit_area", "SO91 0NY", True),
        ("valid_single_digit_area", "BR1 0NY", True),
        ("valid_single_digit_area_WC", "WC1A 0NY", True),
        ("valid_pc_lower", "sw1w 0ny", True),
        ("valid_pc_upper", "SW1W 0NY", True),
        ("valid_london_pc_EC", "EC2A 0NY", True),
        ("valid_london_pc_SE1", "SE1P 0NY", True),
        ("valid_london_pc_NW1", "NW1W 0NY", True),
        ("valid_london_pc_N1", "N1C 0NY", True),
        ("valid_london_pc_E1", "E1W 0NY", True),
        ("valid_london_pc_WC2", "WC2A 0NY", True),
    ])
    def test_valid_pc_returns_true(self, test_case, postcode, expected):

        passed = UkPostcode(postcode).is_valid()

        self.assertTrue(passed)
