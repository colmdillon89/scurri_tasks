import re


def is_integer(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class InvalidPostcode(Exception):
    pass


class UkPostcode:

    def __init__(self, postcode):
        """
        A postcode utility class to validate and format UK postcodes.

        :param postcode: (str) postcode
        """

        self.postcode = postcode
        self.area_code = None
        self.district_code = None
        self.sector = None
        self.unit = None

    def validate_postcode(self):
        validation_tests = {
            "ALL": (
                self._check_first_position_letters,
                self._check_second_position_letters,
                self._check_third_position_letters,
                self._check_fourth_position_letters,
                self._check_final_two_letters,
                self._check_sectors,

            ),
            ('BR', 'FY', 'HA', 'HD', 'HG', 'HR', 'HS', 'HX', 'JE', 'LD', 'SM', 'SR', 'WC', 'WN', 'ZE'):
                (
                    self._check_single_digit_areas,
                ),
            ('AB', 'LL', 'SO'):
                (
                    self._check_double_digit_areas,
                ),
            ('BL', 'BS', 'CM', 'CR', 'FY', 'HA', 'PR', 'SL', 'SS'):
                (
                    self._check_district_zero,
                ),
            ('EC', 'SW', 'W', 'WC', 'E', 'N', 'NW', 'SE'):
                (
                self._check_london_central_area_districts,
                ),
        }

        passed = False

        for k, v in validation_tests.items():
            if k == "ALL" or self.area_code in k:
                for test in v:
                    try:
                        passed = test()
                    except AssertionError as e:
                        raise InvalidPostcode(e)

        return passed

    def is_valid(self):
        """
        Checks if the uk postcode is valid against the
        regex (https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting)
        :return:
        """

        if not isinstance(self.postcode, str):
            return False

        self.postcode = self.postcode.strip().upper()

        self._extract_pc_elements()

        return self.validate_postcode()

    def format(self):
        """
        Formats the postcode.
        :return: (str) the formatted postcode.
        """
        if not self.is_valid():
            raise InvalidPostcode("Invalid postcode ({}) could not format."
                                  "".format(self.postcode))

        return self.postcode

    def _extract_pc_elements(self):
        """
        Extracts the elements of a postcode.
        :return: (dict)
        """
        inward = self.postcode[-3:]

        if not re.fullmatch('[0-9][A-Z]{2}', inward):
            raise InvalidPostcode("Invalid inward section of postcode")

        self.unit = inward[1:3]
        self.sector = inward[0]

        if " " not in self.postcode:
            self.postcode = self.postcode[:-3] + " " + inward

        ac_matches = re.match(r"[A-Z]{1,2}", self.postcode[:2])

        self.area_code = ac_matches.group() if ac_matches else None

        if not self.area_code:
            raise InvalidPostcode("Could not find a valid area code.")

        self.district_code = self.postcode[len(self.area_code):-4]

        if not re.fullmatch(r'[0-9]{1,2}|[0-9][A-Z]', self.district_code):
            raise InvalidPostcode("Could not find a valid district code.")

    def _check_first_position_letters(self):
        """
        The letters Q, V and X are not used in the first position.
        :return: bool
        """
        msg = "The letters Q, V and X are not used in the first position."
        assert self.area_code[0] not in ['Q', 'V', 'X'], msg

        return True

    def _check_second_position_letters(self):
        """
        The letters I, J and Z are not used in the second position.
        :return: bool
        """
        msg = "The letters I, J and Z are not used in the second position."

        if len(self.area_code) > 1:
            assert self.area_code[1] not in ['I', 'J', 'Z'], msg

        return True

    def _check_third_position_letters(self):
        """
        The only letters to appear in the third position are A, B, C, D, E, F, G, H, J, K, P, S, T, U and W when
        the structure starts with A9A
        :return: bool
        """
        msg = "The only letters to appear in the third position are A, B, C, D, E, F, G, H, J, K, P, S, T, U and " \
              "W when the structure starts with A9A"

        if len(self.area_code) == 1 and re.fullmatch('[0-9][A-Z]', self.district_code):
            assert self.district_code[1] in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                             'J', 'K', 'P', 'S', 'T', 'U', 'W'], msg

        return True

    def _check_fourth_position_letters(self):
        """
        The only letters to appear in the fourth position are A, B, E, H, M, N, P, R, V, W, X and Y when the
        structure starts with AA9A.
        :return: bool
        """
        msg = "The only letters to appear in the fourth position are A, B, E, H, M, N, P, R, V, W, X and Y when the " \
              "structure starts with AA9A."

        if len(self.area_code) == 2 and re.fullmatch(r'[0-9][A-Z]', self.district_code):
            assert self.district_code[1] in ['A', 'B', 'E', 'H', 'M', 'N', 'P', 'R', 'V', 'W', 'X', 'Y'], msg

        return True

    def _check_final_two_letters(self):
        """
        The final two letters do not use C, I, K, M, O or V, so as not to resemble digits or each other when hand-written.
        :return: bool
        """
        msg = "The final two letters do not use C, I, K, M, O or V, so as not to resemble digits or " \
              "each other when hand-written."

        for c in self.unit:
            assert c not in ['C', 'I', 'K', 'M', 'O', 'V'], msg

        return True

    def _check_sectors(self):
        """
        Postcode sectors are one of ten digits: 0 to 9
        :return: bool
        """
        msg = "Postcode sectors are one of ten digits: 0 to 9"

        try:
            s = int(self.sector)
            assert 0 <= s <= 9, msg
        except ValueError:
            raise AssertionError(msg)

        return True

    def _check_single_digit_areas(self):
        """
        Areas with only single-digit districts: BR, FY, HA, HD, HG, HR, HS, HX, JE, LD, SM, SR, WC, WN, ZE
        (although WC is always subdivided by a further letter, e.g. WC1A)
        :return: bool
        """
        msg = "Areas with only single-digit districts: BR, FY, HA, HD, HG, HR, HS, HX, JE, LD, SM, SR, WC, WN, ZE" \
              "(although WC is always subdivided by a further letter, e.g. WC1A)"

        if self.area_code == "WC":
            assert re.fullmatch(r"\d[A-Z]", self.district_code), msg
        else:
            assert re.fullmatch(r"\d", self.district_code), msg

        return True

    def _check_double_digit_areas(self):
        """
        Areas with only double-digit districts: AB, LL, SO
        :return: bool
        """
        msg = "Areas with only double-digit districts: AB, LL, SO"

        assert re.fullmatch(r"\d{2}", self.district_code), msg

        return True

    def _check_district_zero(self):
        """
        Areas with a district '0' (zero): BL, BS, CM, CR, FY, HA, PR, SL, SS
        (BS is the only area to have both a district 0 and a district 10)
        :return: bool
        """
        msg = "Areas with a district '0' (zero): BL, BS, CM, CR, FY, HA, PR, SL, SS " \
              "(BS is the only area to have both a district 0 and a district 10)"

        outward_code = self.area_code + self.district_code

        assert re.fullmatch(r"(BS(10|0)|[A-Z]{2}0)", outward_code), msg

        return True

    def _check_london_central_area_districts(self):
        """
        The following central London single-digit districts have been further divided by inserting a letter after the digit
        and before the space: EC1–EC4 (but not EC50), SW1, W1, WC1, WC2 and parts of E1 (E1W), N1 (N1C and N1P), NW1 (NW1W)
        and SE1 (SE1P).
        :return: bool
        """

        msg = "The following central London single-digit districts have been further divided by inserting a letter " \
              "after the digit and before the space: EC1–EC4 (but not EC50), SW1, W1, WC1, WC2 and parts of E1 " \
              "(E1W), N1 (N1C and N1P), NW1 (NW1W) and SE1 (SE1P)."

        passed = True

        if self.area_code == 'EC' and 1 <= int(self.district_code[0]) <= 4:
            passed = re.fullmatch(r"[A-Z]", self.district_code[-1])
        elif self.area_code + self.district_code[0] == 'SE1':
            passed = self.district_code == '1P'
        elif self.area_code + self.district_code[0] == 'NW1':
            passed = self.district_code == '1W'
        elif self.area_code + self.district_code[0] == 'N1':
            passed = re.fullmatch(r'1C|1P', self.district_code)
        elif self.area_code + self.district_code[0] == 'E1':
            passed = self.district_code == '1W'
        elif self.area_code + self.district_code[0] in ['SW1', 'W1', 'WC1', 'WC2']:
            passed = re.fullmatch(r'[0-9][A-Z]', self.district_code)

        assert passed, msg

        return True
