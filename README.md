###### Author: Colm Dillon

#### Scurri technical test

###### 1.One thing I am proud of in my career.

In my most recent experience with Datamine, I had a client who wanted a more cost effective approach for 
processing property sales data. Before now this process had been manual and costly for the client. 
    I worked on a solution in AWS which allowed to process sales asynchronously using AWS Lambda, SNS and API Gateway. 
Using these services together allowed the client to push sales data into a serverless environment where it could 
validate and process the sales at minimal cost to the client. 
    One of the problems we had with this approach was a difficulty testing the overall workflow, validating that the Lambda functions were working correctly as a 
group (7-8 functions altogether). I ended up building a number of acceptance tests using the django test 
functionality which automated the testing of these workflows. This gave us timely feedback on whether any recent 
changes had broken the workflow and greatly increased productivity. The feedback from some of the clients 
employees around the ease of use of the new system left me with a great feeling of accomplishment.

###### 2.Three Five program

Run `pip install -r requirements.txt`

Run the program using ``python three_five.py``

###### 3.UK postcode validator library

Can be found in uk_postcodes.py

Run ```nosetests``` to run all tests in test dir.